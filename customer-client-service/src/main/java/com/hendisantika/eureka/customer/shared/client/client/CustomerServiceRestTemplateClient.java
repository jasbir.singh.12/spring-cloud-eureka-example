package com.hendisantika.eureka.customer.shared.client.client;

import com.hendisantika.eureka.customer.shared.Customer;
import com.hendisantika.eureka.customer.shared.client.controller.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/17
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */

@Component
public class CustomerServiceRestTemplateClient {

    @Autowired
    private RestTemplate restTemplate;

    public MessageWrapper<Customer> getCustomer(int id) {

        Customer customer = restTemplate.exchange(
                "http://customer-service/customer/{id}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Customer>() {
                },
                id).getBody();

        return new MessageWrapper<>(customer, "server called using eureka with rest template");

    }

}