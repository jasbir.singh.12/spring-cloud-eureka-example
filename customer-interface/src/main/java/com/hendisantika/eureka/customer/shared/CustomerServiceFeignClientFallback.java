package com.hendisantika.eureka.customer.shared;

import org.springframework.stereotype.Component;
/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/3/17
 * Time: 8:20 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class CustomerServiceFeignClientFallback implements CustomerServiceFeignClient {

    public Customer getCustomer(int id) {
        return new Customer(12, "Fallback", "Customer");
    }

}
